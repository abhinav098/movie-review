class MoviesController < ApplicationController
  before_action :find_movie, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :index]

  def search
    if params[:search].present?
      @movies=Movie.search(params[:search])
    else
      @movies=Movie.all
    end
  end

  def index
    @movies = Movie.all
  end

  def show
    @reviews=@movie.reviews.order(created_at: :desc)
    if @reviews.blank?
      @avg_reviews=0
    else
      @avg_reviews = @reviews.average(:rating).round(0)
    end
  end

  def new
    @movie =current_user.movies.build
  end

  def edit
  end

  def create
    @movie = current_user.movies.build(movie_params)
      if @movie.save
        redirect_to @movie, notice: 'Movie was successfully created.'
      else
        render 'new'
      end
  end

  def update
    if @movie.update(movie_params)
      redirect_to @movie, notice: 'Movie has been updated successfully.'
    else
      render 'edit'
    end
  end

  def destroy
    @movie.destroy
      redirect_to movies_url, notice: 'Movie was successfully destroyed.'
  end

  private
    def find_movie
      @movie = Movie.find(params[:id])
    end

    def movie_params
      params.require(:movie).permit(:title, :description, :movie_length, :director, :raing, :image)
    end
end
