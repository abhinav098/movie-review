class Movie < ActiveRecord::Base
  searchkick
  belongs_to :user
  has_many :reviews

  validates :title, presence: true,length: { minimum: 1 }
  validates :description, presence: true,length: { minimum: 5 }
  validates :director, presence: true,length: { minimum: 3 }
  validates :movie_length, presence: true
  validates :raing, presence: true
  has_attached_file :image, styles: { medium: "400x600#"}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
